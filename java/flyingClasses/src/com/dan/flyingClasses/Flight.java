package com.dan.flyingClasses;

public class Flight {

    /*Declare class fields to store number of passengers and number of seats.
    The private modifier makes them accessible only from inside the class.
    The public modifier makes them accessible from outside the class also.
     */
    private int passengers, flightNumber, seats = 150;
    private char flightClass;
    private boolean[] isSeatAvailable;

    // Initialization block - this will executed at the beginning of every constructor
    {   // create an array with as many booleans as there are seats
        isSeatAvailable = new boolean[seats];
        // set each bool value to true (default is false)
        for(int i = 0; i < seats; i++)
            isSeatAvailable[i] = true;
    }

    /*Class constructors - handles initial values when object is initialized from class. So each new Flight object
    from Flight class will be initialized with values for some fields. We need the public modifier in order to
    instantiate new Flight objects from outside the class - new Flight();
    */
    public Flight(){
    }

    public Flight(int flightNumber){
        this.flightNumber = flightNumber;
    }

    public Flight(char flightClass){
        this.flightClass = flightClass;
    }

    //Public methods to get and return number of seats and passengers that can be called from the outside
    //instead of accessing the seats and passenger fields directly.
    //Called 'accessors' or 'getters' - they retrieves filed values
    public int getSeats(){
        return seats;
    }

    public int getPassengers(){
        return passengers;
    }

    //Public method called 'mutator' or 'setter'
    public void setSeats(int seats){
        //the seats field will receive the value of seats param
        this.seats = seats;
    }


    //Private method, accessible only from inside the class, called by add1Passenger method.
    private void handleTooMany(){
        System.out.println("Too many!");
    }

    //Public method, accessible from outside the class, used to add passengers
    public void add1Passenger(){
        if(passengers < seats)
            passengers+=1;
        else
            handleTooMany();

    }
    //Method to check if we can combine passengers on current flight with passengers from another flight
    public boolean hasRoom(Flight f2){
        int total = this.passengers + f2.passengers;
        /*Return true if total <= seats - else return false. The return type will be boolean since the
        method is of boolean type
         */
        return total <= seats;
    }

    //Method for creating new flight and combining passengers. Will return Flight object reference
    public Flight createNewWithBoth(Flight f2){
        //Create new flight object reference
        Flight newFlight = new Flight();
        //Change the flight nb of seats to current nb of seats
        newFlight.seats = seats;
        //Combine passengers from both flights
        newFlight.passengers = passengers + f2.passengers;
        //returns newFlight object reference
        return newFlight;
    }
}
