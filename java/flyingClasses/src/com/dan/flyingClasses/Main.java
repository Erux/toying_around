package com.dan.flyingClasses;

public class Main {

    public static void main(String[] args) {
    System.out.println("Creating flight 1");
	Flight flight1 = new Flight();
    System.out.println("Adding 1 passenger to flight 1");
	flight1.add1Passenger();
	System.out.println("No of passengers on flight 1: " + flight1.getPassengers());
	//Compile error - not allowed to access private methods
	//flight1.handleTooMany();

    //Instantiate two new flight objects from flight class
    System.out.println("Creating NY to Dublin and SLC to SF flights.");
    Flight nyToDublin = new Flight();
    Flight slcToSf = new Flight();

    /*One object can have multiple references
    un-referenced objects are destroyed.
     */
    //Add two passengers on nyc flight
    System.out.println("Adding 2 passengers on flight NY to Dublin.");
    nyToDublin.add1Passenger();
    nyToDublin.add1Passenger();

    //This will print 2
    System.out.println("No of passengers on flight NY to Dublin: " + nyToDublin.getPassengers());

    //This will print 0
    System.out.println("No of passengers on flight SLC to SF: " + slcToSf.getPassengers());

    //Change reference for nyToDublin flight to same object reference as slcToSf
    System.out.println("Changing SLC to SF reference to same as NY to Dublin.");
    slcToSf = nyToDublin;

    //This will print 2
    System.out.println("No of passengers on flight SLC to SF: " + slcToSf.getPassengers());

    //Create two flights to LAX
    Flight lax1 = new Flight();
    Flight lax2 = new Flight();
    //Add passengers to both flights

    //Create flight REFERENCE (not new flight!)
    Flight lax3 = null; //lax3 is initialized but doesn't point to any object (null)

    //Combine passengers from lax1 and lax2 if there is enough room.
    if(lax1.hasRoom(lax2))
        //We use flight object lax3 since createNewWithBoth method returns an object reference
        lax3 = lax1.createNewWithBoth(lax2);
    //If lax3 no longer points nu nothing, print success message
    if(lax3 != null)
        System.out.println("Flights combined!");
    //Create new flight bucToAms
    Flight bucToAms = new Flight();
    //Use setSeats mutator method to set number of seats for flightw
    bucToAms.setSeats(150);
    //Print the number of seats by calling the getSeats accessor method
    System.out.println(bucToAms.getSeats());

    //Create new passenger
    Passenger bob = new Passenger();
    bob.setCheckedBags(3);
    System.out.println("Bob created.");
    Passenger jane = new Passenger(2, 3);
    jane.setCheckedBags(3);
    System.out.println("Jane created.");
    Passenger fred = new Passenger(2);
    System.out.println("Fred created.");

    }
}
