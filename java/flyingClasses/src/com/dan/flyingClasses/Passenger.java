package com.dan.flyingClasses;

public class Passenger {
    //If no constructor method is provided, Java will
    // provide a default one (comment added when class had no explicit constructor method defined)

    //Define class fields
    private int checkedBags;
    private int freeBags;
    private double perBagFee;

    //Accessor and mutator methods
    public int getCheckedBags(){return checkedBags;}
    public void setCheckedBags(int checkedBags){
        this.checkedBags = checkedBags;
    }

    public int getFreeBags(){return freeBags;}
    public void setFreeBags(int freeBags){
        this.freeBags = freeBags;
    }

    //Create constructor without arguments - that will be called when instantiating the class without arguments
    public Passenger(){
        System.out.println("Empty constructor called!");
    }

    //Create another constructor, with arguments. This will be called when objects are instantiated with argument. The
    //passed argument will become the value for freeBags
    public Passenger(int freeBags){
        //This confusing crap actually means that the this(freeBavs > 1 ...) call is made to the bagFee constructor
        //it can also be read as this(double freeBags)
        this(freeBags > 1 ? 25.0d : 50.0d);
        this.freeBags = freeBags;
        System.out.println("One argument constructor called");
    }

    public Passenger(int freeBags, int checkedBags){
        //One constructor can call another by using this(parameter list)
        this(freeBags);
        this.checkedBags = checkedBags;
        System.out.println("2 argumnets constructor called");
    }
    //Private constructor used for setting perBagFee at object initialization
    private Passenger(double perBagFee){
        System.out.println("private constructor called!");
        this.perBagFee = perBagFee;
    }
}
