package com.dan.logicaloperators;

public class Main {

    public static void main(String[] args) {
	int a = 20, b = 14, c = 5;
	//"and" operator evaluates true if both sides are true
	if (a > b & b > c)
	    System.out.println("a is greater than c");
	//"or" operator evaluates true if one side is true
    if(a < 0 | c < b)
        System.out.println("this prints because 'or' statement evalutes true for c < b");

    //"exclusive or aka XOR evaluates true if either side is true but different from eachother (true-false | false-true)"
    if(a < 0 ^ b < a)
        System.out.println("Success"); //This will not execute unless xor conditions are met

    //Negation
    if(b != a)
        System.out.println("a and b are not equal");
    boolean done = false;
    if(!done)
        System.out.println("Keep going!");

    //Conditional AND - only execute right side if left side is true
    if(a < 0 && b > 5)
        System.out.println("Weeee!"); //This will not execute since a is > 0, resulting in the left side not being executed
    if(a > 0 && b < c)
        System.out.println("This is awesome!"); //Will not execute since b > c - but at least the right side will get evaluated
    if(a > 0 && b > c)
        System.out.println("Victory!");

    //Conditional OR - only execute right side if left side is false
    if(a < 0 || b > c)
        System.out.println("Testing testing 1 2 3");
    }
}
