package com.dan.calcengine;

/*
 MathEquation class. This is the blueprint for all MathEquation objects.
 */

public class MathEquation {
    // define class fields
    private double leftVal;
    private double rightVal;
    // initialize opCode with initial value 'a'. Way better that the default value of char - '\u0000'
    private char opCode = 'a';
    private double result;

    // empty constructor. Since other constructors are defined,
    // we need this one to be able to create objects while
    // passing 0 arguments.
    public MathEquation(){

    }

    // constructor - provides the ability to create a MathEquation object with a specific opCode as an argument
    public MathEquation(char opCode){
        this.opCode = opCode;
    }
    // Constructor provides ability to create objects with all accepted arguments. Also calls another constructor to
    // set opCode.
    public MathEquation(char opCode, double leftVal, double rightVal){
        this(opCode);
        this.leftVal = leftVal;
        this.rightVal = rightVal;
    }

    // define getters and setters
    public double getLeftVal(){
        return leftVal;
    }
    public void setLeftVal(double leftVal){
        this.leftVal = leftVal;
    }

    public double getRightVal(){
        return rightVal;
    }
    public void setRightVal(double rightVal){
        this.rightVal = rightVal;
    }

    public char getOpCode(){
        return opCode;
    }
    public void setOpCode(char opCode){
        this.opCode = opCode;
    }

    public double getResult(){
        return result;
    }


    // public method used to execute operations on values - can be called from outside class
    public void execute(){
        switch (opCode) {
            case 'a':
                result = leftVal + rightVal;
                break;
            case 's':
                result = leftVal - rightVal;
                break;
            case 'd':
                result = rightVal != 0.0d ? leftVal / rightVal : 0.0d;
                break;
            case 'm':
                result = leftVal * rightVal;
                break;

            default:
                System.out.println("Error - invalid opCode");
                result = 0.0d;
                break;
        }
    }
}
