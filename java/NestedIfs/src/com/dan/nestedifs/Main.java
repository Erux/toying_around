package com.dan.nestedifs;

public class Main {

    public static void main(String[] args) {
        float students = 0.0f;
        float rooms = 4.0f;

        //Print number of students if student number is > 0 or print 'no students'
        if(students > 0.0f) {
            if (rooms > 0.0f)
                System.out.println(students / rooms);
        }
        else
            System.out.println("No students");

        System.out.println("\n** end program **");

    }
}
