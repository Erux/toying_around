package com.dan.looping;

public class Main {

    public static void main(String[] args) {
	int kVal = 5;
	int factorial = 1;

	while(kVal > 1){
	    factorial *= kVal;
	    kVal -= 1;
    }
	System.out.println(factorial);
	// or...

	while(kVal > 1)
		factorial *= kVal--;
	System.out.println(factorial);


	//print the value of iVal and multiply it by 2 each iteration, while iVal is less than 100
	int iVal = 150;
	do {
		System.out.print(iVal);
		System.out.print(" * 2 = ");
		iVal *= 2;
		System.out.println(iVal);
	}while (iVal < 100); //do-while pair ensures at least one iteration will be executed

	//for loop
	for(int bVal = 1; bVal < 100; bVal *= 2)
		System.out.println(bVal);

	//Arrays and looping
	/*
	//initialize an empty array with 3 positions
	float[] theVals = new float[3];
	//add an element to each position
	theVals[0] = 10.0f;
	theVals[1] = 20.0f;
	theVals[2] = 15.0f;
	*/

	float[] theVals = {10.0f, 20.0f, 15.0f};
	float sum = 0.0f;
	//add all elements and print sum
	for(int i = 0; i < theVals.length; i++) {
		System.out.println(sum);
		sum += theVals[i];
	}
	System.out.println("The sum is: " + sum);

	//for-each loop - executes statement once for each member in array
	//handles getting collection length and accessing each value
	sum = 0.0f;
	for(float currentVal: theVals)
		sum += currentVal;
	System.out.println(sum);

	//switch
	int gVal = 11;
	switch (gVal % 2){
		case 0:
			System.out.println(gVal + " is even.");
			break;
		case 1:
			System.out.println(gVal + " is odd.");
			break;
		default:
			System.out.println("Oops it broke!");
			break;
	}

    }
}
