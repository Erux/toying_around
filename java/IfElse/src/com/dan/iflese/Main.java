package com.dan.iflese;

public class Main {

    public static void main(String[] args) {
    //Simple conditions with simple comparison
	int v1 = 10;
	int v2 = 40;

	if(v1 > v2)
	    System.out.println("v1 is bigger");
	else
	    System.out.println("v1 is NOT bigger!");

	//Iterate through array and do something according to conditions
	int[] arr = {12,66,97, 43, 21, 76, 332, 341, 678,23,49, 345, 94, 29};
	for(int i = 0; i < arr.length; i++){
		if(arr[i] % 2 != 0){
			System.out.println(arr[i] + " is not divisible by 2.");
		}
		else{
			System.out.println(arr[i]);
		}
	}


	//Multiple conditions
	int g1 = 10, g2 = 4, diff;
	if(g1 > g2){
		diff = g1 - g2;
		System.out.println("g1 is bigger by " + diff);

	}
	else if(g2 > g1){
		diff = g2 - g1;
		System.out.println("g2 is bigger by " + diff);
	}
	else
		System.out.println("g1 and g2 are equal");

    }



}
