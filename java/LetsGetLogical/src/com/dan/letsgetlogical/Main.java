package com.dan.letsgetlogical;

public class Main {

    public static void main(String[] args) {
	int students = 150;
	int rooms = 0;

	//Remove one '&' for fun divide by zero error if rooms = 0
	//Conditional AND stops evaluation if left side returns false
	if(rooms > 0 && students/rooms > 30)
		System.out.println("Crowded!!");

	System.out.println("\n** end program **");
    }
}
