package com.dan.typeconversion;

public class Main {

    public static void main(String[] args) {
        //type conversion also works
        int iVal = 50;
        float fVal = (float) iVal; //Note that actually converting large int to float can lose precision
        //this should print the float version of the initial int type value
        System.out.println(fVal);
        //implicit type conversion
        int x = 3;
        float y = 7.0f;
        //will print value as float
        System.out.println(x+y);


        //more type conversion
        float floatVal = 1.0f;
        double doubleVal = 4.0d;
        byte byteVal = 7;
        short shortVal = 7;
        long longVal = 5;
        /*
        short result1 = longVal; //This will throw an error. Long values cannot be stored in short type variables
        without and explicit cast
        */
        short result0 = (short)longVal; //This could lead to problems if longVal has a value > short range limit
        System.out.println(result0);
        short result1 = (short)byteVal;
        System.out.println(result1);
        /*
        short result2 = byteVal - longVal; //Again illegal. No implicit conversion from long to short
        */
        short result2 = (short)(byteVal - longVal); //Dangerous for certain values of longVal
        System.out.println(result2);
        /*
        long result3 = longVal - floatVal; //Illegal - can't convert from float to long
        */
        double result3 = longVal - doubleVal;
        System.out.println(result3);
        long result4 = (long)(shortVal - longVal + floatVal + doubleVal); //This will throw an error unless type casting to the larget type (long) before storing the result in a variable
        System.out.println(result4);

        System.out.println("Success!");


    }
}
