package com.dan.conditionalassignment;

public class Main {

    public static void main(String[] args) {
	//Assign a value to a variable based on the result of a condition
    int v1 = 7;
    int v2 = 5;
    /*
    Condition to evaluate is placed before '?' sign (v1 > v2). Return value for 'true' is placed after '?'(v1).
    Return value if condition evaluates to 'false' is placed after ':'(v2).
    So in essence - return v1 if v1 is grater than v2 - else return v2
     */
    int vMax = v1 > v2 ? v1 : v2;
    System.out.println("Result: " + vMax);

    float students = 30;
    float rooms = 4;
    /*
    Return 0 if number of rooms is 0 - else return students / rooms
     */
    float studentsPerRoom = rooms == 0.0f ? 0.0f : students / rooms;
    System.out.println(studentsPerRoom);

    }
}
