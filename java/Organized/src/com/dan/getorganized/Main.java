package com.dan.getorganized;

public class Main {

    public static void main(String[] args) {
        int myVar = 5;
        System.out.println("Hello Get Organized!");
        System.out.println(myVar);
        //this will increment before printing
        System.out.println(++myVar);
        //and it will stay incremented
        System.out.println(myVar);
        //this will increment after printing - obviously
        System.out.println(myVar++);
        //this should print 7
        System.out.println(myVar);
        //end results sequence 5 6 6 6 7
        //also this works as expected
        myVar += 1;
        System.out.println(myVar);
        myVar /= 2 * 2;
        System.out.println(myVar);

    }
}
