from robot import Robot


def standard_routine():
    # get user input for room_clean, name and type
    room_clean_index = int(input('On a scale of 1 to 100 - How clean is your room?\n'))
    name = input('Enter a robot name:\n')
    type_ = input('Please enter type. Decepticon or the other kind?\n')
    print('Creating robot to clean your room...')
    # create a new robot with the listed attributes - now is when the constructor is called and data is set
    robot = Robot(name, type_, 10, room_clean_index)
    # now we make our robot instance use its power_on() method and the magic starts
    robot.power_on()


standard_routine()

