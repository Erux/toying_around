import time
import sys


# blueprint for robots - all robot objects will be instances of the Robot class
class Robot:
    # Constructor method - called by default when robot object is instantiated
    def __init__(self, name, type_, battery_level, room_clean_index):
        # instance vars will be populated with input values at initialization
        self.name = name
        self.type_ = type_
        self.battery_level = battery_level
        self.room_clean_index = room_clean_index

    def power_on(self):
        print('\nInitializing Robot:')
        time.sleep(2)
        print('\nRobot name: {}'.format(self.name))
        time.sleep(2)
        print('\nRobot type: {}'.format(self.type_))
        time.sleep(2)
        print('\nRobot battery level: {}'.format(self.battery_level))
        time.sleep(2)
        print('\nRoom clean index: {}'.format(self.room_clean_index))
        time.sleep(2)
        print('\nPurpose: I clean room!\n')
        time.sleep(2)
        # call check_room_clean() method after printing initial information
        self.check_room_clean()

    def mop(self):
        actions = ["dips mop into bucket", "washes floor with wet mop", "computes detergent level",
                   "surveys the room for dirty spots", "makes beeping noises! Beeep! Beeep! Buzz!"]
        for action in actions:
            print("The robot {}".format(action))
            self.battery_level -= 1
            time.sleep(3)
            self.room_clean_index += 2
            print('Room clean index at: {}'.format(self.room_clean_index))
        # call check_battery_level after vacuming.
        self.check_battery_level()

    def vacuum(self):
        for i in range(2):
            print('Using vacuum on furniture and floors')
            self.battery_level -= 3
            time.sleep(2)
            self.room_clean_index += 20
            print('Room clean index at: {}'.format(self.room_clean_index))
        # call check_battery_level after vaccuming.
        self.check_battery_level()

    def charge_robot(self):
        print("Charging robot...\n")
        while self.battery_level < 100:
            time.sleep(1)
            print('Battery level at: {}'.format(self.battery_level))
            self.battery_level += 10
        print("Battery charged at {}. Exiting charge mode.".format(str(self.battery_level)))

    def search_for_docking(self):
        print("Robot {} Going to docking station...".format(self.name))
        time.sleep(3)
        print('Engaging docking station')
        time.sleep(2)
        # call charge_robot() method after docking
        self.charge_robot()
        print('Docking station disengaged.')
        time.sleep(2)

    def check_battery_level(self):
        # if battery level is < 6 call search_for_docking() method
        if self.battery_level < 6:
            time.sleep(2)
            print("\nWarning! Battery level at {}! Charging sequence activated".format(self.battery_level))
            self.search_for_docking()
        else:
            # call check_room_clean() method if battery is charged
            self.check_room_clean()

    def check_room_clean(self):
        # call mop() and vacuum() methods if room clean index is less than 100
        # check battery level after cleaning is done
        if self.room_clean_index <= 100:
            print('Starting mop program:\n')
            self.mop()
            print('Mopping done! Bip Bop..\nStarting vacuum program:')
            self.vacuum()
            print('Vacuum done.')
            self.check_battery_level()
        else:
            # Exit program if room does not need cleaning
            print('Room is clean! I have no purpose :(')
            sys.exit()









