import json
import requests
import argparse
from testrail import *



'''
Testrail python logger
If you wish to extend this script, the testrail API manual might come in handy: 
http://docs.gurock.com/testrail-api2/start 
'''


def main(user, password_token, project_id, suite_id, local, test_report="test_report.json"):

    # authenticate with Testrail API before anything else
    client = APIClient('https://testrail/')
    client.user = user
    client.password = password_token

    # create a new test plan using specified project and test suite
    result = json.dumps(client.send_post('add_plan/{}'.format(project_id), {'name': 'Smoke Test - Created using Testrail API logger',
                                                                            'description': 'Auto generated test results',
                                                                            'entries': [{'suite_id':suite_id, 'include_all': True}]}))

    # extract plan id and run id from testrail response
    data = json.loads(result)
    plan_id = data['entries'][0]['runs'][0]['plan_id']
    run_id = data['entries'][0]['runs'][0]['id']

    # set test case results based on test report
    with open(test_report, "r") as f:
        results = json.load(f)
    client.send_post('add_results_for_cases/{}'.format(run_id), results)

    # open chrome and display test results if [local] flag is set to true
    if local:
        import webbrowser
        test_run = 'https://testrail/index.php?/plans/view/{}'.format(plan_id)
        webbrowser.get("C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s").open(test_run)
    else:
        print("New test run created at: https://testrail/index.php?/plans/view/{}".format(plan_id))




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Testrail api logging script")
    parser.add_argument("-r", "--test-report", type=str, required=True, default="###", help="test report json file")
    parser.add_argument("-u", "--user", type=str, required=True, default="###", help="testrail user email address")
    parser.add_argument("-t", "--password_token", type=str, required=True, default ="###", help="testrail password token")
    parser.add_argument("-p", "--project-id", type=str, required=True, default="###", help="testrail project id")
    parser.add_argument("-s", "--suite-id", type=str, required=True, default="###", help="testrail suite id")
    parser.add_argument("-l", "--local", type=bool, required=False, default=False, help="set to True or False to open testrail to display results")
    args = parser.parse_args()
    main(test_report=args.test_report,user=args.user, password_token=args.password_token, project_id=args.project_id, suite_id=args.suite_id, local=args.local)
