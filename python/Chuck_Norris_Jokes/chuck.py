import requests
import random
import time


# base url for composing requests
base_url = "http://api.icndb.com/"
# SSL verifcation parameter, we set it here to control the state of the verify variable all throughout the script
verify = False

# fetch two random jokes
r = requests.get(base_url+ "jokes/random/2", verify=verify)

# since resonse will be in binary format - let's convert it to a json for easy access
r = r.json()

# extract the values of the 'joke' keys from the response
for joke in r['value']:
    print(joke['joke'])

# fetch the list of joke categories from the API
r = requests.get(base_url + "/categories", verify=verify)
r = r.json()

# extract list of categories and store it in a variable
category_list = r['value']

# unpack the list and print result to console
print("Categories found: ")
print(*category_list)

# select a random category from the categories list
category = category_list[random.randint(0, len(category_list)-1)]
print("Printing random joke from {} category: ".format(category))

# request a ranom joke from that category
r = requests.get(base_url + "/jokes/random?limitTo=[{}]".format(category))
r = r.json()

# and use a 'slow type' effect to print it to the console
for i in r['value']['joke']:
    print(i, end='')
    time.sleep(random.uniform(0.1, 0.3))
    


