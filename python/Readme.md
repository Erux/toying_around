
# Classesss:
A simple project to check I understood the basic concepts of python classes:

    - A a bluieprint class (Robot)

    - A main.py file that imports the class, instantiates robot and calls methods

    - init.py to make it importable


# Collatz

Write a function named collatz() that has one parameter named number. If number is even, then collatz() should print number // 2 and return this value.
If number is odd, then collatz() should print and return 3 * number + 1.
Then write a program that lets the user type in an integer and that keeps calling collatz() on that number until the function returns the value 1.
(Amazingly enough, this sequence actually works for any integer—sooner or later, using this sequence, you’ll arrive at 1!
Even mathematicians aren’t sure why. 
Your program is exploring what’s called the Collatz sequence, sometimes called “the simplest impossible math problem.”)


# Chuck Norris Jokes

Interacting with a public REST API server - that generates random Chuck Norris jokes.
Used requests, random and time modules to performe requests, parse response bodies and print extracted data to console.


# Testrail logger

Testrail logging script. Can be used to create a test run in testrail, based on a specified project and suite id, and set results for test cases - based on a test report json file.

## Getting Started

Download the testrail_logger.py script and testrail.py module in the same folder on local drive.

### Prerequisites

Requests and argparse modules are also required to run the logger

```text
pip install requests
pip install argparse
```
## Running the script

The following arguments are required for running this script:
```
-r - test_report.json file (please see test report example)
```
```
-u - testrail user email address (e.g. "user@domain.com")
```
```
-t - testrail password token (e.g. "eFyTETew9hlOnytxAaOkfgcNPzGC1ksgGxUB7cPHM")
```
```
-p - testrail project id
```
```
-s - testrail suite id
```
```
-l - OPTIONAL argument; Opens Chrome and navigates to Testrail testrun url (requires webdriver python module)
```
Example:
```text
python testrail_logger.py -r "test_report.json" -u "usere@domain.com" -t "eFyTETew9hlOnytxAaOkfgcNPzGC1ksgGxUB7cPHM" -p "340" -s "191145"
```

### Example of valid test_report.json file:
```json
{
	"results": [
		{
			"case_id": 25291963,
			"status_id": 5,
			"comment": "This test failed"
		},
		{
			"case_id": 25292040,
			"status_id": 1,
			"comment": "This test passed",
		}
	]
}
```
The case_id field can be extracted from the testrail test suite. They remain static until the test case is deleted.
E.g.: For https://testrail/index.php?/cases/view/25292040 the case_id will be 25292040.


### Extending this script:
The testrail API manual might be usefull for this purpose:
http://docs.gurock.com/testrail-api2/start


## Author
ddan.costache@gmail.com